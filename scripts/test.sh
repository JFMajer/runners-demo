#!/bin/bash

URL='http://localhost'

status=$(curl --head --location --connect-timeout 5 --write-out %{http_code} --silent --output /dev/null ${URL})

echo $status
